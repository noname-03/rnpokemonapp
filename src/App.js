import { View, Text, SafeAreaView, TouchableOpacity } from 'react-native'
import React from 'react'
import { Formik } from 'formik'
import Form from './Screen/Form/Register'
import Router from './Router'
import CodePush from 'react-native-code-push'

const CodePushOptions = { checkFrequency: CodePush.CheckFrequency.ON_APP_START }


const App = () => {
  return (
    <Router />
  )
}

export default CodePush(CodePushOptions)(App)