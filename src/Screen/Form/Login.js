import React, { useEffect, useState } from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    Alert,
    TextInput,
    StyleSheet,
    ActivityIndicator
} from 'react-native'
import { COLORS } from '../../Styles/Color';
import database from '@react-native-firebase/database';
import Input from './Input'
import { Formik } from 'formik'
import * as yup from 'yup'


const Login = ({ navigation }) => {

    const [loading, setLoading] = useState(false)

    const handleSubmit = values => {
        console.log(values)
        setLoading(true)
        try {
            database()
                .ref('users/')
                .orderByChild("emailId")
                .equalTo(values.email)
                .once('value')
                .then(async snapshot => {
                    if (snapshot.val() == null) {
                        Alert.alert("MAAF", "Email tidak ditemukan, Harap periksa kembali email anda")
                        return false;
                    }
                    let userData = Object.values(snapshot.val())[0];
                    if (userData?.password != values.password) {
                        Alert.alert("Error", "Invalid Password!");
                        return false;
                    }
                    console.log('User data: ', userData);
                    console.log('Sukses');
                    setLoading(false)
                    // navigation.navigate('Home', { userData: userData })
                    navigation.navigate('Home')
                });
        } catch (error) {
            setLoading(false)
            Alert.alert('Error', 'Not Found User')
        }
    }
    const signUpValidationSchema = yup.object().shape({
        email: yup
            .string()
            .email("Please enter valid email")
            .required('Email is required'),
        password: yup
            .string()
            .matches(/\w*[a-z]\w*/, "Password must have a small letter")
            .matches(/\w*[A-Z]\w*/, "Password must have a capital letter")
            .matches(/\d/, "Password must have a number")
            .matches(/[!@#$%^&*()\-_"=+{}; :,<.>]/, "Password must have a special character")
            .min(8, ({ min }) => `Password must be at least ${min} characters`)
            .required('Password is required'),
    })


    return (

        <Formik
            validationSchema={signUpValidationSchema}
            initialValues={{ email: '', password: '' }}
            onSubmit={handleSubmit}>
            {
                ({ handleChange, handleBlur, handleSubmit, values, errors, isValid }) => (

                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <View style={[styles.inputContainer, { marginTop: 10 }]}>
                            <Input
                                style={styles.inputs}
                                placeholder={'Email'}
                                onChangeText={handleChange('email')}
                                error={errors.email}
                                value={values.email}
                            />
                        </View>
                        <View style={[styles.inputContainer, { marginTop: 10 }]}>
                            <Input
                                placeholder={'Password'}
                                onChangeText={handleChange('password')}
                                secureTextEntry={true}
                                error={errors.password}
                                value={values.password}
                            />
                        </View>
                        <TouchableOpacity
                            style={styles.btn}
                            disabled={!isValid}
                            onPress={() => handleSubmit()}
                        >
                            {
                                loading ?
                                    <ActivityIndicator color={COLORS.white} />
                                    :
                                    <Text style={styles.btnText}>Login</Text>
                            }
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => navigation.navigate('Register')}>
                            <Text style={{ color: 'blue' }}>Register</Text>
                        </TouchableOpacity>
                    </View>
                )
            }

        </Formik >
    )
}

export default Login

const styles = StyleSheet.create({
    inputs: {
        borderBottomColor: COLORS.white,
        color: COLORS.black,
        paddingLeft: 10,
        flex: 1
    },
    inputContainer: {
        borderRadius: 30,
        height: 48,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: COLORS.white,
        marginBottom: 10,
        elevation: 2,
        borderColor: COLORS.green,
        borderWidth: 2,
        width: '90%'
    },
    btnText: {
        color: '#fff',
        fontSize: 14,
        marginTop: 2,
    },
    btn: {
        marginTop: 12,
        backgroundColor: COLORS.theme,
        width: '90%',
        height: 50,
        borderRadius: 30,
        elevation: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
})
