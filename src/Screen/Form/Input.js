import React from 'react'
import { View, TextInput, Text, Dimensions, StyleSheet } from 'react-native'
import { COLORS } from '../../Styles/Color'

const Input = ({ onChangeText, value, placeholder, error, secureTextEntry }) => {
    const win = Dimensions.width
    return (
        <View style={{ marginVertical: 100, height: 82, width: win }}>
            <TextInput
                style={styles.InputContainer}
                onChangeText={onChangeText}
                value={value}
                placeholder={placeholder}
                secureTextEntry={secureTextEntry}
            />
            <Text style={{ color: 'red' }}>{error}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    InputContainer: {
        alignSelf: 'flex-start',
        borderBottomColor: COLORS.white,
        color: COLORS.liteBlack,
        paddingLeft: 10,
        flex: 1
    }
})

export default Input