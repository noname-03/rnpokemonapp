import React, { useState } from 'react'
import { View, Text, SafeAreaView, TouchableOpacity, StyleSheet, Alert, ActivityIndicator } from 'react-native'
import { Formik } from 'formik'
import Input from './Input'
import { COLORS } from '../../Styles/Color'
import database from '@react-native-firebase/database';
import * as yup from 'yup'
import uuid from 'react-native-uuid';


const Register = ({ navigation }) => {

    const [loading, setLoading] = useState(false)

    const handleSubmit = values => {
        console.log(values.email)
        setLoading(true)
        if (values.name == '' || values.email == '' || values.password == '') {
            Alert.alert('Error', 'Harap isi Semua field')
            return false;
        }
        let data = {
            id: uuid.v4(),
            name: values.name,
            emailId: values.email,
            password: values.password,
        };
        try {
            database()
                .ref('/users/' + data.id)
                .set(data)
                .then(() => {
                    setLoading(false)
                    Alert.alert('Success', 'Register Successfully!');
                    navigation.navigate('Login')
                });
        } catch (error) {
            setLoading(false)
            Alert.alert('Error', error)
        }
    }
    const signUpValidationSchema = yup.object().shape({
        name: yup
            .string()
            .matches(/^[A-Za-z ]*$/, 'Please enter valid name')
            .required('Name is required'),
        email: yup
            .string()
            .email("Please enter valid email")
            .required('Email is required'),
        password: yup
            .string()
            .matches(/\w*[a-z]\w*/, "Password must have a small letter")
            .matches(/\w*[A-Z]\w*/, "Password must have a capital letter")
            .matches(/\d/, "Password must have a number")
            .matches(/[!@#$%^&*()\-_"=+{}; :,<.>]/, "Password must have a special character")
            .min(8, ({ min }) => `Password must be at least ${min} characters`)
            .required('Password is required'),
    })

    return (
        <Formik
            validationSchema={signUpValidationSchema}
            initialValues={{ name: '', email: '', password: '' }}
            onSubmit={handleSubmit}>
            {
                ({ handleChange, handleBlur, handleSubmit, values, errors }) => (

                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <View style={[styles.inputContainer, { marginTop: 10 }]}>
                            <Input
                                style={styles.inputs}
                                placeholder={'nama'}
                                onChangeText={handleChange('name')}
                                error={errors.name}
                                value={values.name}
                            />
                        </View>
                        <View style={[styles.inputContainer, { marginTop: 10 }]}>
                            <Input
                                style={styles.inputs}
                                placeholder={'Email'}
                                onChangeText={handleChange('email')}
                                error={errors.email}
                                value={values.email}
                            />
                        </View>
                        <View style={[styles.inputContainer, { marginTop: 10 }]}>
                            <Input
                                placeholder={'Password'}
                                onChangeText={handleChange('password')}
                                secureTextEntry={true}
                                error={errors.password}
                                value={values.password}
                            />
                        </View>
                        <TouchableOpacity
                            style={styles.btn}
                            onPress={() => handleSubmit()}
                        >
                            {
                                loading ?
                                    <ActivityIndicator color={COLORS.white} />
                                    :
                                    <Text style={styles.btnText}>Register</Text>
                            }
                        </TouchableOpacity>
                    </View>
                )
            }

        </Formik >
    )
}

export default Register

const styles = StyleSheet.create({
    inputs: {
        borderBottomColor: COLORS.white,
        color: COLORS.liteBlack,
        paddingLeft: 10,
        flex: 1
    },
    inputContainer: {
        borderRadius: 30,
        height: 48,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: COLORS.white,
        marginBottom: 10,
        elevation: 2,
        borderColor: COLORS.green,
        borderWidth: 2,
        width: '90%'
    },
    btnText: {
        color: '#fff',
        fontSize: 14,
        marginTop: 2,
    },
    btn: {
        marginTop: 12,
        backgroundColor: COLORS.theme,
        width: '90%',
        height: 50,
        borderRadius: 30,
        elevation: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
})
