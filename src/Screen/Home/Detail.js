import { View, Text, Image, Alert, Dimensions, TouchableOpacity, ActivityIndicator } from 'react-native'
import React, { useEffect, useState } from 'react'
import axios from 'axios'
import database from '@react-native-firebase/database';
import uuid from 'react-native-uuid';
import { COLORS } from '../../Styles/Color'

const Detail = ({ route }) => {

    const [loading, setLoading] = useState(false)
    const { id } = route.params;
    const [Name, setName] = useState('');
    const [Height, setHeight] = useState('');
    const [Weight, setWeight] = useState('');
    const [Type, setType] = useState('');
    const [Photo, setPhoto] = useState({});
    const windowWidth = Dimensions.get('window').width;
    const windowHeight = Dimensions.get('window').height;

    const fetchData = async () => {
        try {
            const results = await axios.get(
                `https://pokeapi.co/api/v2/pokemon/${id}/`
            );
            // console.log(results.data)
            setName(results.data.species.name)
            setPhoto(results.data.sprites.other.home.front_default)
            // console.log(results.data.sprites.other.home.front_default)
            setHeight(results.data.height)
            setWeight(results.data.weight)
            setType(results.data.types[0].type.name)
            // console.log(results.data);
        } catch (err) {
            Alert.alert('ERROR MESSAGE', `${err}`);
        }
    };


    useEffect(() => {
        fetchData();
        // console.log(Data.sprites.other.home.front_default)
    }, []);

    const handleSubmit = values => {
        // console.log(values.email)
        setLoading(true)
        let data = {
            id: uuid.v4(),
            name: Name,
            photo: Photo,
            idPokemon: id,
        };
        try {
            database()
                .ref('Bag/')
                .orderByChild("idPokemon")
                .equalTo(id)
                .once('value')
                .then(async snapshot => {
                    if (snapshot.val() == null) {
                        console.log('data akan dimasukan')
                        database()
                            .ref('/Bag/' + data.id)
                            .set(data)
                            .then(() => {
                                setLoading(false)
                                Alert.alert('Success', 'Successfully!');
                                // navigation.navigate('Login')
                            });
                    } else {
                        Alert.alert('Error', 'Data Sudah Ada.!')
                        console.log('data sudah ada')
                    }
                    setLoading(false)
                });
            // database()
            //     .ref('/Bag/' + data.id)
            //     .set(data)
            //     .then(() => {
            //         setLoading(false)
            //         Alert.alert('Success', 'Successfully!');
            //         // navigation.navigate('Login')
            //     });
        } catch (error) {
            setLoading(false)
            Alert.alert('Error', error)
        }
    }

    return (
        <View style={{ flex: 1 }}>
            <Text style={{ alignSelf: 'center', fontWeight: 'bold', padding: 4 }}>Detail</Text>
            <Image
                source={{ uri: `${Photo}` }}
                style={{ margin: 12, width: 200, height: 200, alignSelf: 'center' }}
            />
            <View style={{ backgroundColor: 'red', height: windowHeight, borderRadius: 8 }}>
                <Text style={{ padding: 20, fontWeight: 'bold' }}>Nama : {Name}</Text>
                <Text style={{ padding: 20, fontWeight: 'bold' }}>Tinggi {Height}</Text>
                <Text style={{ padding: 20, fontWeight: 'bold' }}>Bobot : {Weight}</Text>
                <Text style={{ padding: 20, fontWeight: 'bold' }}>Type : {Type}</Text>
                <TouchableOpacity
                    onPress={() => handleSubmit()}
                    style={{ alignSelf: 'center', backgroundColor: 'blue', padding: 14, borderRadius: 8 }}>
                    {
                        loading ?
                            <ActivityIndicator color={COLORS.white} />
                            :
                            // <Text style={styles.btnText}>Register</Text>
                            <Text style={{ color: 'white' }}>Catch</Text>
                    }
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default Detail