import { View, Text, FlatList, Image, TouchableOpacity } from 'react-native'
import React, { useEffect, useState } from 'react'
import database from '@react-native-firebase/database';

const Bag = ({ navigation }) => {

    const [Data, setData] = useState([])

    useEffect(() => {
        getData();
    }, []);

    const getData = () => {
        database()
            .ref('Bag/')
            .once('value')
            .then(snapshot => {
                // console.log('all User data: ', Object.values(snapshot.val()));
                // setData('all User data: ', Object.values(snapshot.val()));
                setData(Object.values(snapshot.val()))
            });
    };

    console.log(Data)
    const list = () => {
        return Data.map((element) => {
            return (
                <View style={{ margin: 10, backgroundColor: 'grey' }}>
                    {/* <Text>{element.idPokemon}</Text> */}
                    <TouchableOpacity onPress={() => {
                        navigation.navigate('Detail', {
                            id: `${element.idPokemon}`,
                        })
                    }}>
                        <Text>Nama : {element.name}</Text>
                        <Image
                            source={{ uri: `${element.photo}` }}
                            style={{ margin: 12, width: 100, height: 100, alignSelf: 'center' }}
                        />
                    </TouchableOpacity>
                    {/* <Text>{element.photo}</Text> */}
                    {/* <Text>{element.subtitle}</Text> */}
                </View>
            );
        });
    };
    return (
        <View>
            <Text style={{ color: 'red', alignSelf: 'center', fontWeight: 'bold' }}>Bag</Text>
            {list()}
        </View>
    )
}

export default Bag