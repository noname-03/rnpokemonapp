import React, { useState, useCallback, useEffect } from "react";
import { StyleSheet, Text, View, FlatList, Image, Dimensions, TouchableOpacity } from "react-native";
import axios from 'axios'

// const pokePath = "https://pokeapi.co/api/v2/";
// const pokeQuery = "pokemon?offset=0&limit=6";
// const firstGenPokemonPath = `${pokePath}${pokeQuery}`;
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

//1 Call Pokemon IDs --> 151 = 152 Calls to the API

export default function Home({ navigation }) {

    const [firstGenPokemonDetails, setfirstGenPokemonDetails] = useState([]);
    const [Data, setData] = useState([]);
    const [Offset, setOffset] = useState(0);


    useEffect(() => {
        const fetchFirstGenPokemons = async () => {
            const firstGenPokemonIdsResponse = await fetch(`https://pokeapi.co/api/v2/pokemon?offset=${Offset}&limit=6`);
            const firstGenPokemonIdsBody = await firstGenPokemonIdsResponse.json();
            setData(firstGenPokemonIdsBody)
            // console.log(firstGenPokemonIdsBody)
            // const = firstGenPokemonIdsBody.next
            // console.log(firstGenPokemonIdsBody.next)
            // console.log(firstGenPokemonIdsBody.previous)

            const firstGenPokemonDetails = await Promise.all(
                firstGenPokemonIdsBody.results.map(async (p) => {
                    const pDetails = await fetch(p.url);

                    return await pDetails.json();
                })
            );

            setfirstGenPokemonDetails(firstGenPokemonDetails);
        };

        fetchFirstGenPokemons();
        // getApi()
    }, []);
    console.log(firstGenPokemonDetails)
    const renderPokemon = ({ item }) => {
        return (
            <View style={styles.pokemonContainer}>
                <TouchableOpacity onPress={() => {
                    navigation.navigate('Detail', {
                        id: `${item.id}`,
                    })
                }}
                >
                    <Text style={styles.pokemonTitle}>
                        {item.name.charAt(0).toUpperCase() + item.name.slice(1)}
                    </Text>
                    <Image
                        style={styles.pokemonSprite}
                        source={{
                            uri: item.sprites.front_default,
                        }}
                    />
                </TouchableOpacity>
            </View>
        );
    };

    return (
        <View style={styles.container}>
            <Text style={styles.title}>First Gen Pokemons</Text>
            <FlatList
                data={firstGenPokemonDetails}
                renderItem={renderPokemon}
                numColumns={2} />
            <View style={{ flexDirection: 'row' }}>
                <TouchableOpacity
                    onPress={() => setOffset(Offset - 6)}
                    style={{ backgroundColor: 'red', margin: 14, padding: 8 }}>
                    <Text style={{ color: 'white' }}>Prev</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => navigation.navigate('Bag')}
                    style={{ backgroundColor: 'blue', margin: 14, padding: 8 }}>
                    <Text style={{ color: 'white' }}>BAG</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => onRefresh()}
                    // onPress={() => setOffset(Offset + 6)}
                    style={{ backgroundColor: 'red', margin: 14, padding: 8 }}>
                    <Text style={{ color: 'white' }}>Next</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: 'center'
        // marginTop: 60
    },
    title: {
        fontSize: 38,
        alignSelf: "center",
        marginBottom: 20,
    },
    pokemonContainer: {
        width: windowWidth / 2,
        height: 200,
        backgroundColor: "lightgrey",
        margin: 4
    },
    pokemonTitle: {
        fontSize: 20,
        alignSelf: "center",
        marginTop: 10,
    },
    pokemonSprite: {
        width: 100,
        height: 100,
        alignSelf: "center",
    },
});